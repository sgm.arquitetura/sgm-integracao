package br.com.pbd.sgm.integracao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class IntegracaoApplication {

    public static void main(String[] args) {
        SpringApplication.run(IntegracaoApplication.class, args);
    }

    @Bean
    public RouteLocator customRouteLocator(RouteLocatorBuilder builder) {
        return builder.routes()
                .route("stur-imoveis", r -> r.path("/stur/imoveis/**").uri("http://localhost:8090/"))
                .route("stur-debitos", r -> r.path("/stur/debitos/**").uri("http://localhost:8090/"))
                .build();
    }
}
